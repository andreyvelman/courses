from django.db import models
from django.contrib.auth.models import User, AbstractUser

from datetime import datetime, date


class CustomUser(AbstractUser):
    date_of_birth = models.DateField("Date of birth (mm/dd/yyyy)", null=True)
    phone = models.CharField(max_length=13)
    country = models.CharField(max_length=30)

    def user_group(self):
        return self.groups.filter(name='Moderators').exists()

    def user_can_delete_course(self):
        return self.groups.filter(name='Admins').exists()


class Course(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    text = models.TextField()
    price = models.DecimalField(decimal_places=2, max_digits=2)
    subscriber = models.ManyToManyField(CustomUser)
    category = models.ForeignKey('core.Category', null=True, on_delete=models.SET_NULL, related_name='courses')

    def __str__(self):
        return self.title


class Comment(models.Model):
    text = models.TextField(verbose_name='Комментарий')
    created_at = models.DateTimeField(auto_now_add=True)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='comment_course')
    commentator = models.ForeignKey(CustomUser, related_name='comment_customuser', on_delete=models.SET_NULL, null=True)


class Lecture(models.Model):
    title = models.CharField(max_length=200)
    text = models.TextField()
    course = models.ForeignKey(Course, null=True, on_delete=models.SET_NULL)


class Subscriber(models.Model):
    sub = models.IntegerField(default=0)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='subscribers')


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name
