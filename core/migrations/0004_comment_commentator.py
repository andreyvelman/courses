# Generated by Django 3.2.9 on 2022-02-09 14:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20220130_1528'),
    ]

    operations = [
        migrations.AddField(
            model_name='comment',
            name='commentator',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='comment_customuser', to=settings.AUTH_USER_MODEL),
        ),
    ]
