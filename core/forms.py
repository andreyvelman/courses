from django import forms
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.forms.widgets import Textarea

from core.models import Comment, Course
# # from django.contrib.auth import login


class RegistrationForm(forms.ModelForm):

    confirm_password = forms.CharField(widget=forms.widgets.PasswordInput())

    class Meta:
        model = get_user_model()
        fields = ['username', 'password', 'confirm_password', 'email']
        widgets = {'password': forms.widgets.PasswordInput}

    def clean_confirm_password(self):
        if self.cleaned_data['confirm_password'] != self.cleaned_data['password']:
            raise forms.ValidationError('Password do not match')

        return self.cleaned_data['password']

    def clean(self):
        validate_password(self.cleaned_data['password'])
        return super().clean()

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class UserProfileEditForm(forms.ModelForm):
    class Meta:
        model = get_user_model()
        fields = ['username', 'email', 'phone', 'date_of_birth', 'country']


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ['text']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for fields in self.fields:
            self.fields[fields].widget.attrs['class'] = 'form-control'
        self.fields['text'].widget = Textarea(attrs={'rows':5})


class CourseCreationForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['title', 'description', 'text', 'price', 'category']
