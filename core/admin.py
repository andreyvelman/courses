from django.contrib import admin
from core.models import Category, Course, CustomUser

admin.site.register(Category)
admin.site.register(Course)
admin.site.register(CustomUser)