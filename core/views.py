from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, FormView, UpdateView, RedirectView, CreateView, DeleteView
from django.views.generic.edit import FormMixin
from core.models import Category, Course, CustomUser
from core.forms import RegistrationForm, UserProfileEditForm, CommentForm, CourseCreationForm
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.db.models import Count


class IndexView(ListView):
    template_name = 'index.html'
    model = Category

    def get_courses(self):
        cat_id = self.request.GET.get('c_id')

        if cat_id:
            return Course.objects.annotate(s_count=Count('subscriber')).filter(category_id=cat_id).order_by('-s_count')

        return Course.objects.annotate(s_count=Count('subscriber')).order_by('-s_count')[:10]

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        context['courses'] = self.get_courses()

        return context


class CourseDetailView(FormMixin, DetailView):
    model = Course
    template_name = 'CourseDetailView.html'
    form_class = CommentForm

    def already_subscribed(self):
        return self.object.subscriber.filter(id=self.request.user.id).exists()

    def get_context_data(self, **kwargs):
        context = super(CourseDetailView, self).get_context_data()
        context['is_subscribed'] = self.already_subscribed()
        return context

    def form_valid(self, form):
        comment = form.save(commit=False)
        comment.commentator = self.request.user
        comment.course = self.get_object()
        comment.save()
        return super().form_valid(form)

    def post(self, requset, *args, **kwargs):
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_success_url(self, *args, **kwargs):
        return reverse_lazy('course_detail', kwargs={'pk': self.get_object().id})


class Subscribers(RedirectView):

    def get_redirect_url(self, *args, **kwargs):
        return reverse_lazy('course_detail', args=[self.request.POST.get('course_id')])

    def post(self, request, *args, **kwargs):
        course = get_object_or_404(Course, id=self.request.POST.get('course_id'))
        if request.POST.get('action') == 'subscribe':
            course.subscriber.add(self.request.user)
        else:
            course.subscriber.remove(self.request.user)
        return super().post(request, *args, **kwargs)


class RegistrationView(FormView):
    template_name = 'registration.html'
    form_class = RegistrationForm
    success_url = '/login/'

    def form_valid(self, form):
        form.save()
        return super(RegistrationView, self).form_valid(form)


class UserProfileView(DetailView):
    model = CustomUser
    template_name = 'profile.html'

    def get_course(self):
        return Course.objects.filter(subscriber=self.object)

    def get_context_data(self, **kwargs):
        context = super(UserProfileView, self).get_context_data()
        context['l_courses'] = self.get_course()
        return context


class UserProfileEditView(UpdateView):
    model = get_user_model()
    form_class = UserProfileEditForm
    template_name = 'profile_edit.html'
    success_url = '/'


class CourseCreationView(CreateView):
    template_name = 'course_creation.html'
    form_class = CourseCreationForm
    success_url = '/'


class CourseDeleteView(DeleteView):
    model = Course
    success_url = '/'


class CourseUpdateView(UpdateView):
    model = Course
    template_name = 'course_creation.html'
    form_class = CourseCreationForm
    success_url = '/'