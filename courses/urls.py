"""courses URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib import admin
from django.urls import path
from core import views

# app_name = 'core'

urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('admin/', admin.site.urls),
    path('design/', views.IndexView.as_view(), name='design'),
    path('login/', LoginView.as_view(template_name='login.html'), name='login'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('registration/', views.RegistrationView.as_view(), name='registration'),
    path('profile/<int:pk>/', views.UserProfileView.as_view(), name='profile'),
    path('profile/edit/<int:pk>/', views.UserProfileEditView.as_view(), name='profile-edit'),
    path('course/<int:pk>/detail/', views.CourseDetailView.as_view(), name='course_detail'),
    path('course_creation/', views.CourseCreationView.as_view(), name='course_creation'),
    path('course_delete/<int:pk>/', views.CourseDeleteView.as_view(), name='course_delete'),
    path('course_update/<int:pk>/', views.CourseUpdateView.as_view(), name='course_update'),
    path('subscribed/', views.Subscribers.as_view(), name='subscriber')

]